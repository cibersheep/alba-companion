/*
 * Copyright (C) 2021  Joan CiberSheep
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * albacompanion is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.9
import Ubuntu.Components 1.3
import "Components" as Components
import "js/utils.js" as Utils

Page {
    anchors.fill: parent

    header: Components.HeaderBase {
        id: header
        title: "Locations"
    }

    Flickable {
        contentHeight: contentItem.childrenRect.height

        anchors {
            fill: parent
            topMargin: parent.header.height
        }

        Column {
            width: parent.width

            Label {
                width: parent.width
                height: Math.max(contentHeight, units.gu(6))

                anchors {
                    horizontalCenter: parent.horizontalCenter
                    margins: units.gu(2)
                }

                text: "Tap to mark as unlocked, swipe for options"
                wrapMode: Text.WordWrap
                horizontalAlignment: Text.AlignHCenter
                verticalAlignment: Text.AlignVCenter
                color: theme.palette.normal.backgroundSecondaryText
            }

            Repeater {
                id: repeater
                clip: true

                model: gameSettings.locations
                delegate: locationsDelegate
            }
        }
    }

    Component {
        id: locationsDelegate

        ListItem {
            width: parent.width
            divider.visible: false
            clip: true
            highlightColor: selectionColor

            trailingActions: ListItemActions {
                actions: [
                    Action {
                        iconName: gameSettings.locationsCompleted[index] === "0"
                            ? "toolkit_tick"
                            : "toolkit_cross"

                        text: "Completed"
                        enabled: gameSettings.locationsUnlocked[index] == "1"

                        onTriggered: {
                            var completed = false
                            if (gameSettings.locationsCompleted[index] === "0") {
                                completed = true
                            }

                            gameSettings.locationsCompleted = Utils.setBit(gameSettings.locationsCompleted, index, completed)
                        }
                    },
                    Action {
                        iconSource: gameSettings.locationsColored[index] !== "R"
                            ? "../assets/red-circle.svg"
                            : "../assets/red-clear.svg"

                        text: "Avoid"
                        enabled: gameSettings.locationsUnlocked[index] == "1"

                        onTriggered: {
                            if (gameSettings.locationsColored[index] !== "R") {
                                gameSettings.locationsColored = Utils.setString(gameSettings.locationsColored, index, "R")
                            } else {
                                gameSettings.locationsColored = Utils.setString(gameSettings.locationsColored, index, "0")
                            }
                        }
                    },
                    Action {
                        iconSource: gameSettings.locationsColored[index] !== "G"
                            ? "../assets/green-circle.svg"
                            : "../assets/green-clear.svg"

                        text: "Save"
                        enabled: gameSettings.locationsUnlocked[index] == "1"

                        onTriggered: {
                            if (gameSettings.locationsColored[index] !== "G") {
                                gameSettings.locationsColored = Utils.setString(gameSettings.locationsColored, index, "G")
                            } else {
                                gameSettings.locationsColored = Utils.setString(gameSettings.locationsColored, index, "0")
                            }
                        }
                    }
                ]
            }

            ListItemLayout {
                title.text: modelData
                title.font.strikeout: gameSettings.locationsCompleted[index] == "1"

                CheckBox {
                    id: locationsOwned
                    SlotsLayout.position: SlotsLayout.Trailing

                    onCheckedChanged: gameSettings.locationsUnlocked = Utils.setBit(gameSettings.locationsUnlocked, index, checked)

                    Component.onCompleted: checked = gameSettings.locationsUnlocked[index] === "1"
                }

                Icon {
                    source: gameSettings.locationsColored[index] === "G"
                        ? "../assets/green-circle.svg"
                        : "../assets/red-circle.svg"
                    width: units.gu(2)
                    height: width
                    visible: gameSettings.locationsColored[index] === "G" || gameSettings.locationsColored[index] === "R"
                    SlotsLayout.position: SlotsLayout.Leading
                }
            }

            onClicked: locationsOwned.checked = !locationsOwned.checked
        }
    }

    Component.onDestruction: mainStack.pageDestructed()

    Component.onCompleted: mainStack.isPagePushed = true
}
