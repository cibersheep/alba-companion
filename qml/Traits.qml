/*
 * Copyright (C) 2021  Joan CiberSheep
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * albacompanion is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.9
import Ubuntu.Components 1.3
import "Components" as Components
import "js/utils.js" as Utils

Page {
    anchors.fill: parent

    header: Components.HeaderBase {
        id: header
        title: "Traits"
    }

    Flickable {
        contentHeight: contentItem.childrenRect.height

        anchors {
            fill: parent
            topMargin: parent.header.height + units.gu(2)
        }

        Grid {
            id: grid
            width: parent.width
            columns: 2
            columnSpacing: units.gu(2)

            Repeater {
                id: repeater
                clip: true

                model: gameSettings.traits
                delegate: specDelegate
            }
        }
    }

    Component {
        id: specDelegate

        Column {
            width: (grid.width - units.gu(3)) / 2
            spacing: units.gu(2)

            Button {
                anchors.horizontalCenter: parent.horizontalCenter
                width: parent.width - units.gu(6)
                height: width

                Text {
                    width: parent.width
                    text: modelData
                    horizontalAlignment: Text.AlignHCenter
                    color: theme.palette.normal.activityText

                    anchors {
                        centerIn: parent
                        verticalCenterOffset: -units.gu(4)
                    }
                }

                Text {
                    width: parent.width
                    text: "+"
                    horizontalAlignment: Text.AlignHCenter
                    color: theme.palette.normal.activityText

                    anchors {
                        centerIn: parent
                        verticalCenterOffset: units.gu(2)
                    }
                }

                onClicked: gameSettings.traitsChecked = Utils.addTo(gameSettings.traitsChecked, index, 1)
            }

            Button {
                text: "-"
                anchors.horizontalCenter: parent.horizontalCenter

                onClicked: gameSettings.traitsChecked = Utils.addTo(gameSettings.traitsChecked, index, -1)
            }

            Text {
                height: units.gu(6)
                text: gameSettings.traitsChecked.split(",")[index]
                horizontalAlignment: Text.AlignHCenter
                color: theme.palette.normal.backgroundText

                anchors.horizontalCenter: parent.horizontalCenter
            }
        }
    }

    Component.onDestruction: mainStack.pageDestructed()

    Component.onCompleted: mainStack.isPagePushed = true
}
