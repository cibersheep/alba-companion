/*
 * Copyright (C) 2021  Joan CiberSheep
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * albacompanion is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.9
import Ubuntu.Components 1.3
import "../js/utils.js" as Utils

ListItem {
    property var stringVar
    signal updateVal(string Var)

    width: parent.width
    divider.visible: false
    clip: true
    highlightColor: selectionColor

    trailingActions: ListItemActions {
        actions: Action {
            iconName: "remove"

            text: "Completed"
            enabled: Number(stringVar.split(",")[index]) > 0

            onTriggered: stringVar = Utils.addTo(stringVar, index, -1)
        }
    }

    ListItemLayout {
        title.text: stringVar.split(",")[index] + "  " + modelData

        Icon {
            name: "add"
            width: units.gu(2)
            height: width
            SlotsLayout.position: SlotsLayout.Trailing
        }
    }

    onClicked: {
        updateVal(Utils.addTo(stringVar, index, 1))
        }
}
