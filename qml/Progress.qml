/*
 * Copyright (C) 2021  Joan CiberSheep
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * albacompanion is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.9
import Ubuntu.Components 1.3
import Ubuntu.Components.Popups 1.3

import "Components" as Components

Page {
    id: progressPage
    anchors.fill: parent

    header: Components.HeaderProgress {
        id: header
        title: "Progress"

        onAddSection: PopupUtils.open(addDialog)
    }

    Flickable {
        contentHeight: contentItem.childrenRect.height

        anchors {
            fill: parent
            topMargin: parent.header.height
        }

        Column {
            width: parent.width

            Repeater {
                clip: true

                model: gameSettings.currentSection.split(",")
                delegate: progressDelegate
            }
        }
    }

    Component {
        id: progressDelegate

        ListItem {
            width: parent.width
            divider.visible: false
            clip: true
            highlightColor: selectionColor

            ListItemLayout {
                title.text: "Section: " + modelData
            }
        }
    }

    Component {
        id: addDialog

        Dialog {
            id: dialog
            title: "Add a section"
            text: "Add a section to your progress"

            TextField {
                id: input
                inputMethodHints: Qt.ImhFormattedNumbersOnly
            }

            Button {
                text: "Add"
                color: theme.palette.normal.positive
                enabled: !!input.text

                onClicked: {
                    addProgress(input.text)
                    PopupUtils.close(dialog)
                }
            }

            Button {
                text: "Cancel"
                onClicked: PopupUtils.close(dialog)
            }
        }
    }

    function addProgress(point) {
        gameSettings.currentSection = gameSettings.currentSection + "," + point
    }

    Component.onDestruction: mainStack.pageDestructed()

    Component.onCompleted: mainStack.isPagePushed = true
}
