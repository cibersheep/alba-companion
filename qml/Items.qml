/*
 * Copyright (C) 2021  Joan CiberSheep
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * albacompanion is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.9
import Ubuntu.Components 1.3
import "Components" as Components

Page {
    anchors.fill: parent

    header: Components.HeaderBase {
        id: header
        title: "Items"
    }

    Flickable {
        contentHeight: contentItem.childrenRect.height

        anchors {
            fill: parent
            topMargin: parent.header.height
        }

        Column {
            width: parent.width

            Label {
                width: parent.width
                height: Math.max(contentHeight, units.gu(6))

                anchors {
                    horizontalCenter: parent.horizontalCenter
                    margins: units.gu(2)
                }

                text: "Tap to add an item, swipe for remove"
                wrapMode: Text.WordWrap
                horizontalAlignment: Text.AlignHCenter
                verticalAlignment: Text.AlignVCenter
                color: theme.palette.normal.backgroundSecondaryText
            }

            Label {
                width: parent.width - units.gu(6)
                height: Math.max(contentHeight, units.gu(6))

                anchors {
                    horizontalCenter: parent.horizontalCenter
                    margins: units.gu(2)
                }

                text: "You cannot lose these items at any point, you always have them"
                wrapMode: Text.WordWrap
                horizontalAlignment: Text.AlignHCenter
                verticalAlignment: Text.AlignVCenter
            }

            Repeater {
                id: defaultItems
                clip: true

                model: gameSettings.defaultItems
                delegate: defaultItemsDelegate
            }

            Label {
                width: parent.width
                height: Math.max(contentHeight, units.gu(6))

                anchors {
                    horizontalCenter: parent.horizontalCenter
                    margins: units.gu(2)
                }

                text: "Items"
                wrapMode: Text.WordWrap
                horizontalAlignment: Text.AlignHCenter
                verticalAlignment: Text.AlignVCenter
            }

            Repeater {
                id: repeater
                clip: true

                model: gameSettings.items
                delegate: itemsDelegate
            }

            Label {
                width: parent.width
                height: Math.max(contentHeight, units.gu(6))

                anchors {
                    horizontalCenter: parent.horizontalCenter
                    margins: units.gu(2)
                }

                text: "Prologue Items"
                wrapMode: Text.WordWrap
                horizontalAlignment: Text.AlignHCenter
                verticalAlignment: Text.AlignVCenter
            }

            Repeater {
                id: prologueItems
                clip: true

                model: gameSettings.prologueItems
                delegate: prologueItemsDelegate
            }
        }
    }

    Component {
        id: defaultItemsDelegate

        ListItem {
            width: parent.width
            divider.visible: false
            clip: true
            highlightColor: selectionColor

            ListItemLayout {
                title.text: modelData
            }
        }
    }

    Component {
        id: itemsDelegate

        Components.GeneralDelegate {
            stringVar: gameSettings.itemsOwned

            onUpdateVal: gameSettings.itemsOwned = Var
        }
    }

    Component {
        id: prologueItemsDelegate

        Components.GeneralDelegate {
            stringVar: gameSettings.prologueItemsOwned

            onUpdateVal: gameSettings.prologueItemsOwned = Var
        }
    }

    Component.onDestruction: mainStack.pageDestructed()

    Component.onCompleted: mainStack.isPagePushed = true
}
