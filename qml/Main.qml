/*
 * Copyright (C) 2021  Joan CiberSheep
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * albacompanion is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.9
import Ubuntu.Components 1.3
import Qt.labs.settings 1.0

import "Components" as Components
import "js/utils.js" as Utils

MainView {
    id: root
    objectName: 'mainView'
    applicationName: 'albacompanion.cibersheep'
    automaticOrientation: true

    signal ready

    Component.onCompleted: ready()

    //Colors
    property string   lighterColor: UbuntuColors.porcelain
    property bool        darkTheme: theme.name === "Ubuntu.Components.Themes.SuruDark"
    property string selectionColor: "#33fd244a"
    property string     lightColor: "#ec266b"
    property string      darkColor: "#ff733d"

    width: units.gu(45)
    height: units.gu(75)

    Settings {
        id: appSettings
        category: "App Settings"

        property string currentTheme: ""
    }

    Settings {
        id: gameSettings
        category: "Game Settings"

        //Current position
        property var     currentSection: "1.0"

        //Character Sheet
        property string         yourName: ""
        property string         boatName: ""
        property var               names: ["Marlin (N.1)", "Ark (N.2)", "Speedwell (N.3)"]
        property var        namesChecked: "000"

        //Perks
        property var               perks: ["Shower (P.1)", "Caravan Trade (P.2)", "Irradiated (P.3)"]
        property string   perksCollected: "000"
        property var           perksUsed: "000"

        //Specialities
        property var        specialities: ["Engineering (S.1)", "Old World Language (S.2)", "Hand-to-Hand Combat (S.3)", "Knowledge (S.4)",
                                          "Chemistry (S.5)", "Medicine (S.6)", "Survival (S.7)", "Sharpshooter (S.8)"  ]
        property var specialitiesChecked: "00000000"

        //Traits
        property var              traits: ["Resolve", "Compassion", "Instability", "Cruelty"]
        property var       traitsChecked: "0,0,0,0" //Max values? 24 might be just random

        //Encounters
        property var          encounters: ["The Hermit (E.1)", "Morag (E.2)", "Naomi the Medic (E.3)", "Charlie the Mechanic (E.4)",
                                           "Altercation with Phil (E.5)", "Crucible Tom (E.6)", "Priestly Encounter (E.7)", "Windmill Warden (E.8)",
                                           "Gaia (E.9)", "Gaia Seen (E.10)", "Rescue Gaia (E.11)", "Gaia in Tow (E.12)",
                                           "Knowledge of Hidden Raft (E.13)", "Meet Dagger (E.14)", "Dagger’s Fate (E.15)", "Viktor (E.16)",
                                           "Talk with Boy (E.17)", "Ship’s Guards (E.18)", "The Gaoler (E.19)", "Balloon Sabotage (E.20)",
                                           "The Balloon (E.21)"]
        //                                -    |    |    |    |
        property var   encountersChecked:  "000000000000000000000"

        //Items
        property var        defaultItems: ["Compass", "Map", "Bedroll", "Tent"]
        property var               items: ["Morphine (O.1)", "Good Quality Rations (O.2)", "Lamp & Kerosene (O.3)", "Hatchet (O.4)", "Power Plant Files (O.5)",
                                           "High Quality Rations (O.6)", "Film (O.7)", "Balloon Blueprints (O.8)", "Uranium (O.9)", "Game Knowledge (O.10)",
                                           "Ear (O.11)", "Note from Dagger (O.12)", "Dagger’s Dagger (O.13)", "Deer Skin (O.14)", "Statue Sketch (O.15)",
                                           "Dagger’s Letter (O.16)", "Jewellery (O.17)", "Radio for Idiots (O.18)", "Bear Tooth (O.19)", "Tiger Skin (O.20)",
                                           "Tiger Claw (O.21)", "Gaia’s Badge (O.22)", "Ship’s Token (O.23)", "Fresh Meat (O.24)", "Prie Robes (O.25)",
                                           "Military Pin (O.26)", "Radium (O.27)", "Balloon Blueprints (O.28)"]
        property var          itemsOwned:  "0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0"

        property var       prologueItems: ["Vola’s Necklace (O.29)", "Nice Coat (O.30)", "Pocket Knife (O.31)", "Nice Rations (O.32)"]
        property var  prologueItemsOwned: "0,0,0,0"

        //Locations
        property var           locations: ["Base Camp (L.1)", "The Storm (L.2)", "The Power plant (L.3)", "Return to the Power Plant (L.4)",
                                           "The Ship (L.5)", "Return to the Ship (L.6)", "The Raid (L.7)", "The Great Migration (L.8)",
                                           "The Scavenger Village (L.9)", "Return to the Scavenger Village (L.10)", "The Bridge (L.11)",
                                           "The Zoo (L.12)", "The City Centre (L.13)", "The Church (L.14)", "The Uranium Heist (L.15)",
                                           "The Cell (L.16)", "The Library (L.17)", "The Base/The Offer (L.18)", "Return to the Base (L.19)",
                                           "The Push (L.20)", "The Open Fields (L.21)", "The Metro (L.22)", "The Windmill (L.23)",
                                           "The Caravan (L.24)", "The Radio Tower (L.25)", "The Battlefield (L.26)", "The Aftermath (L.27)",
                                           "The Streets (L.28)", "The Reunion (L.29)", "The Seaside Farm (L.S1)", "The Cult (L.S2)"]
        //                                -    |    |    |    |    |
        property var   locationsUnlocked: "0000000000000000000000000000000"
        property var  locationsCompleted: "0000000000000000000000000000000"
        property var    locationsColored: "0000000000000000000000000000000"

        property var          fieldNotes: ["Mess tent (F.1)", "Cottage Sketch (F.2)", "Heart (F.3)", "Grease Stain (F.4)", "Sketch of Tree (F.5)",
                                           "Pressed Flower (F.6)", "Curious Animal (F.7)", "Outfit Sketch (F.8)", "Frog (F.9)", "Squashed Frog (F.10)",
                                           "Duck Curtain (F.11)", "Smiley Face (F.12)", "Bird Prints (F.13)", "Berries (F.14)", "DO NOT EAT (F.15)",
                                           "Bloody Shrapnel (F.16)", "Radio Tower Coordinates (F.17)", "Balloon Sketch (F.18)", "Wild Mushrooms (F.19)",
                                           "Cloud Drawings (F.20)", "Metro Map (F.21)", "Sketch of Wires (F.22)", "Courgettes & Tomatoes (F.23)",
                                           "Scavenger Map (F.24)", "Vault Badge (F.25)", "Scavenger’s Artefact (F.26)", "Seafood (F.29)", "Incense (F.30)"]
        property var     fieldNotesFound: "0000000000000000000000000000"

        Component.onCompleted: {
            if (locationsUnlocked.length == 29) {
                console.log("Updating vars")
                locationsUnlocked = locationsUnlocked + "00"
                locationsCompleted = locationsCompleted + "00"
                locations.push("The Seaside Farm (L.S1)", "The Cult (L.S2)")
            } else if (locationsUnlocked.length == 30) {
                console.log("Updating vars (30)")
                locationsUnlocked = locationsUnlocked + "0"
                locationsCompleted = locationsCompleted + "0"
                locations.push("The Cult (L.S2)")
            }
        }
    }

    AdaptivePageLayout {
        id: mainStack

        property bool isPagePushed: false
        signal pageDestructed

        onPageDestructed: if (columns < 2) {
            isPagePushed = false
        }

        anchors.fill: parent
        primaryPage: mainPage

        onColumnsChanged: pageToPush()

        Component.onCompleted: pageToPush()

        function pageToPush() {
            if (columns > 1 && !isPagePushed) {
                isPagePushed = true
                mainStack.addPageToNextColumn(mainPage, Qt.resolvedUrl("Progress.qml"))
            }
        }

        layouts: PageColumnsLayout {
            id: pcl
            when: width > units.gu(52)

            PageColumn {
                minimumWidth: units.gu(25)
                maximumWidth: units.gu(45)
                preferredWidth: units.gu(40)
            }

            PageColumn {
                id: rightColumn
                fillWidth: true
            }
        }
    }

    Page {
        id: mainPage
        anchors.fill: parent

        header: Components.HeaderBase {
            id: header
            title: "Alba Companion"
        }

        UbuntuListView {
            id: mainListView

            header: charactersheetHeader
            highlight: Rectangle { color: "transparent" }

            anchors {
                fill: parent
                topMargin: parent.header.height
            }

            clip: true

            model: characterSheet
            delegate: characterSheetDelegate

            Component.onCompleted: currentIndex = -1
        }
    }

    ListModel {
        id: characterSheet

        Component.onCompleted: append([
            { section: "Encounters",   page: "Encounters.qml"   },
            { section: "Field Notes",  page: "Notes.qml"        },
            { section: "Items",        page: "Items.qml"        },
            { section: "Locations",    page: "Locations.qml"    },
            { section: "Perks",        page: "Perks.qml"        },
            { section: "Specialities", page: "Specialities.qml" },
            { section: "Traits",       page: "Traits.qml"       }
        ])
    }

    Component {
        id: characterSheetDelegate

        ListItem {
            width: mainListView.width
            divider.visible: false
            clip: true
            highlightColor: selectionColor

            ListItemLayout {
                title.text: section
                title.color: mainListView.currentIndex == index
                    ? lightColor
                    : theme.palette.normal.backgroundSecondaryText
                title.font.bold: mainListView.currentIndex === index

                ProgressionSlot {}
            }

            onClicked: {
                mainListView.currentIndex = index
                mainStack.addPageToNextColumn(mainPage, Qt.resolvedUrl(page))
            }
        }
    }

    Component {
        id: charactersheetHeader

        Column {
            Connections {
                    target: root

                onReady: {
                    name0.checked = gameSettings.namesChecked[0] === "1"
                    name1.checked = gameSettings.namesChecked[1] === "1"
                    name2.checked = gameSettings.namesChecked[2] === "1"
                }
            }

            anchors {
                left: parent.left
                right: parent.right
                margins: units.gu(1)
            }

            Row {
                width: parent.width
                height: units.gu(6)
                spacing: units.gu(2)

                Label {
                    id: gamename
                    text: "Your Name:"
                    height: parent.height
                    verticalAlignment: Text.AlignVCenter
                }

                TextField {
                    width: parent.width - gamename.width - units.gu(2)
                    anchors.verticalCenter: parent.verticalCenter
                    text: gameSettings.yourName
                    placeholderText: "Type your name"

                    onTextChanged: gameSettings.yourName = text
                }
            }

            ListItem {
                width: parent.width
                divider.visible: false
                clip: true
                highlightColor: selectionColor

                ListItemLayout {
                    title.text: "Current position: " + gameSettings.currentSection.split(",")[gameSettings.currentSection.split(",").length -1]

                    ProgressionSlot {}
                }

                onClicked: mainStack.addPageToNextColumn(mainPage, Qt.resolvedUrl("Progress.qml"))
            }

            Label {
                id: boatname
                text: "Boat Name:"
                width: parent.width
                height: units.gu(6)
                verticalAlignment: Text.AlignVCenter
            }

            ListItem {
                width: parent.width
                divider.visible: false
                clip: true
                highlightColor: selectionColor

                ListItemLayout {
                    title.text: gameSettings.names[0]

                    CheckBox {
                        id: name0
                        SlotsLayout.position: SlotsLayout.Trailing

                        onCheckedChanged: gameSettings.namesChecked = Utils.setBit(gameSettings.namesChecked, 0, checked)
                    }
                }

                onClicked: name0.checked = !name0.checked
            }

            ListItem {
                width: parent.width
                divider.visible: false
                clip: true
                highlightColor: selectionColor

                ListItemLayout {
                    title.text: gameSettings.names[1]

                    CheckBox {
                        id: name1
                        SlotsLayout.position: SlotsLayout.Trailing

                        onCheckedChanged: gameSettings.namesChecked = Utils.setBit(gameSettings.namesChecked, 1, checked)
                    }
                }

                onClicked: name1.checked = !name1.checked
            }

            ListItem {
                width: parent.width
                divider.visible: false
                clip: true
                highlightColor: selectionColor

                ListItemLayout {
                    title.text: gameSettings.names[2]

                    CheckBox {
                        id: name2
                        SlotsLayout.position: SlotsLayout.Trailing

                        onCheckedChanged: gameSettings.namesChecked = Utils.setBit(gameSettings.namesChecked, 2, checked)
                    }
                }

                onClicked: name2.checked = !name2.checked
            }
        }
    }
}
