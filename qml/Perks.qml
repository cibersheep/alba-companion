/*
 * Copyright (C) 2021  Joan CiberSheep
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * albacompanion is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.9
import Ubuntu.Components 1.3
import "Components" as Components
import "js/utils.js" as Utils

Page {
    anchors.fill: parent

    header: Components.HeaderBase {
        id: header
        title: "Perks"
    }

    Flickable {
        contentHeight: contentItem.childrenRect.height

        anchors {
            fill: parent
            topMargin: parent.header.height
        }

        Column {
            width: parent.width

            Label {
                width: parent.width
                height: Math.max(contentHeight, units.gu(6))

                anchors {
                    horizontalCenter: parent.horizontalCenter
                    margins: units.gu(2)
                }

                text: "Tap to mark as collected, swipe for used"
                wrapMode: Text.WordWrap
                horizontalAlignment: Text.AlignHCenter
                verticalAlignment: Text.AlignVCenter
                color: theme.palette.normal.backgroundSecondaryText
            }

            Repeater {
                id: repeater
                clip: true

                model: gameSettings.perks
                delegate: perksDelegate
            }
        }
    }

    Component {
        id: perksDelegate

        ListItem {
            width: parent.width
            divider.visible: false
            clip: true
            highlightColor: selectionColor

            trailingActions: ListItemActions {
            actions: Action {
                    iconName: gameSettings.perksUsed[index] === "0"
                        ? "toolkit_tick"
                        : "toolkit_cross"

                    text: "Used"
                    enabled: gameSettings.perksCollected[index] == "1"

                    onTriggered: {
                        var used = false
                        if (gameSettings.perksUsed[index] === "0") {
                            used = true
                        }

                        gameSettings.perksUsed = Utils.setBit(gameSettings.perksUsed, index, used)
                    }
                }
            }

            ListItemLayout {
                title.text: modelData
                title.font.strikeout: gameSettings.perksUsed[index] == "1"

                CheckBox {
                    id: perksOwned
                    SlotsLayout.position: SlotsLayout.Trailing

                    onCheckedChanged: gameSettings.perksCollected = Utils.setBit(gameSettings.perksCollected, index, checked)

                    Component.onCompleted: checked = gameSettings.perksCollected[index] === "1"
                }
            }

            onClicked: perksOwned.checked = !perksOwned.checked
        }
    }

    Component.onDestruction: mainStack.pageDestructed()

    Component.onCompleted: mainStack.isPagePushed = true
}
