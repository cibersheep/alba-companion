/*
 * Copyright (C) 2021  Joan CiberSheep
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * albacompanion is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.9
import Ubuntu.Components 1.3
import "Components" as Components
import "js/utils.js" as Utils

Page {
    anchors.fill: parent

    header: Components.HeaderBase {
        id: header
        title: "Encounters"
    }

    Flickable {
        contentHeight: contentItem.childrenRect.height

        anchors {
            fill: parent
            topMargin: parent.header.height
        }

        Column {
            width: parent.width

            Repeater {
                id: repeater
                clip: true

                model: gameSettings.encounters
                delegate: encountersDelegate
            }
        }
    }

    Component {
        id: encountersDelegate

        ListItem {
            width: parent.width
            divider.visible: false
            clip: true
            highlightColor: selectionColor

            ListItemLayout {
                title.text: modelData

                CheckBox {
                    id: specChecked
                    SlotsLayout.position: SlotsLayout.Trailing

                    onCheckedChanged: gameSettings.encountersChecked = Utils.setBit(gameSettings.encountersChecked, index, checked)

                    Component.onCompleted: checked = gameSettings.encountersChecked[index] === "1"
                }
            }

            onClicked: specChecked.checked = !specChecked.checked
        }
    }

    Component.onDestruction: mainStack.pageDestructed()

    Component.onCompleted: mainStack.isPagePushed = true
}
