function setBit(string, position, bit) {
    if (position > string.length) {
        console.log("Error: position is out of string range")
        return string
    }

    var newBit = "0"
    if (bit) newBit = "1"

    return string.substring(0, position) + newBit + string.substring(position + 1)
}

function setString(string, position, text) {
    if (position > string.length) {
        console.log("Error: letter position is out of string range")
        return string
    }

    return string.substring(0, position) + text + string.substring(position + 1)
}

function addTo(source, position, amount) {
    var arrayStrings = source.split(",")
    var digit = Number(arrayStrings[position])
    digit = digit + amount
    arrayStrings.splice(position,1,String(digit))

    return arrayStrings.join()
}
